**Reflexion Styger Jonas**

Bei diesem Projekt konnte ich meine Spring Kenntnisse verbessert und konnte das Wissen von 
den Modulen von Herrn Cavuoti anwenden. Zu dem hat die Teamarbeit wunderbar funktioniert. 
Ansonsten konnte ich gut arbeiten und mir hat die Arbeit Spass gemacht.

**Probleme**

Ein Problem war dass ich leider nicht ganz mit Gitlab klar kam und ich konnte aus mir unerklärlichen Gründen
nicht «pushen» konnte. Ein anderes Problem gab es mit den Bildern, wir haben nicht genau
gewusst wie darstellen. Darum haben wir keine Bilder, was ein bisschen schade ist. 
