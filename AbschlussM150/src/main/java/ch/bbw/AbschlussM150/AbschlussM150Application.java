package ch.bbw.AbschlussM150;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbschlussM150Application {

	public static void main(String[] args) {
		SpringApplication.run(AbschlussM150Application.class, args);
	}

}
