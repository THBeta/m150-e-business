package ch.bbw.AbschlussM150;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
public class ArticleDAO {
	
	
	private ArrayList<Article> articles;
	
	public ArticleDAO() {
		articles = new ArrayList<>();
		
		articles.add(new Article(0, "Computer", "Schöner Computer, mit high defintion Monitor, 5 TeraByte und für gaming geeignet", 100.00));
		articles.add(new Article(1, "Ipad", "Ipad mini 2, 128 GB, hochauflösung", 400.00));
		articles.add(new Article(2, "Iphone", "Iphone 13, Beste kamera der Welt, 256 GB", 600.00));
		articles.add(new Article(3, "SSD", "SSD, 1 Terbyte", 200.00));
		articles.add(new Article(4, "Michaels Handy", "Schlechtes honor 7, veraltet und hässlich", 0.10));
		
		
		articles.get(0).setImage("./images/image1.png");
		articles.get(1).setImage("./images/image2.png");
		articles.get(2).setImage("./images/image3.png");
		articles.get(3).setImage("./images/image4.png");
		articles.get(4).setImage("./images/image5.png");
	}
	
	public ArrayList<Article> getArticles() {
		return articles;
	}
	
	public Article getArticleById(int id) {
		
		Article article = null;
		boolean found = false;
		for(Article a : articles) {
			if(id == a.getId()) {
				found = true;
				article = a;
			} 
		}
		
		if(found) {
			return article;
		} else {
			return null;
		}
		
		
		
	}

	/*public Optional<Article> getArticleById(int id) {
		return articles.stream()
			.filter(a -> a.getId()==id)
			.findFirst();
	}*/
	
}
