package ch.bbw.AbschlussM150;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.owasp.validator.html.ScanException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.annotation.RequestScope;



@Controller
@RequestScope
public class ShopController {

	@Autowired
	UserRepository userRepository;
	User user;

	
	@Autowired
	CartDAO cartDAO;

	@Autowired
	ArticleDAO articleDAO;

	@GetMapping("/")
	public String home() {

		return "index";
	}
	
	@GetMapping("/index")
	public String index() {

		return "index";
	}
	
	@GetMapping("/webshop")
	public String webshop(Model model) {

		model.addAttribute("notLogged", "Sie sind nicht eingelogged");
		return "webshop";
	}
	
	@GetMapping("/userPage")
	public String userPage(Model model) {

		model.addAttribute("loginMessage", "Sie sind nicht eingelogged");
		return "userPage";
	}

	@GetMapping("/account")
	public String account() {

		return "account";
	}

	private byte[] passwordToSHAHash(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256"); // Factory Pattern
			md.update(password.getBytes());
			return md.digest();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getHashAsHexString(byte[] password) {
		String output = "";

		for (byte d : password) {
			String nextByte = Integer.toHexString(d & 0xFF);
			if (nextByte.length() < 2) {
				nextByte = "0" + nextByte;
			}
			output += nextByte;
		}
		return output;
	}

	@PostMapping("/login")
	public String login(@ModelAttribute User user, BindingResult result, Model model) throws PolicyException, ScanException {

		
		String dirtyUser = user.getUsername();
		String dirtyPass = user.getPassword();
		

		InputStream policyIn = this.getClass().getClassLoader().getResourceAsStream("antisamy-anythinggoes.xml");
		Policy policy = Policy.getInstance(policyIn);

		AntiSamy as = new AntiSamy();

		CleanResults us = as.scan(dirtyUser, policy);
		CleanResults ps = as.scan(dirtyPass, policy);
		
		boolean userpass = false;
		String username = us.getCleanHTML();
		String password = ps.getCleanHTML();
		byte[] hashPas = passwordToSHAHash(password);
		String stringPas = getHashAsHexString(hashPas);
		
		User userdata = new User();
		
		Iterable<User> ilist = userRepository.findAll();
		
		for (User u : ilist) {
			if (username.equals(u.getUsername())) {
				if(stringPas.equals(u.getPassword())) {
					userdata = u;
					userpass = true;
				}
				
			} 
		}
		
		if(userpass) {
			
			
			model.addAttribute("password", password);
			model.addAttribute("user", userdata);
			model.addAttribute("loginMessage", "Login was succesful");
			return "userPage";
			
		} else {
			
			
			model.addAttribute("errorLogin", "Login was a failure, überprüfen Sie ihren Eingaben");
			return "index";
		}
		
	}
	
	@GetMapping("/logout")
	public String logout(Model model) {
		
		
		Cart cart = cartDAO.getCardById(1);
		for(Article a : articleDAO.getArticles()) {
			a.setAmount(1);
		}
		
		cart.getList().clear();
		model.addAttribute("errorLogin", "You have logged out");
		return "index";
		
	}
		
	

	@PostMapping("/createAccount")
	public String createAccount(@ModelAttribute User user, BindingResult result, Model model) throws PolicyException, ScanException {

		Iterable<User> ilist = userRepository.findAll();
		boolean invalidInput = false;
		
		String dirtyUser = user.getUsername();
		String dirtyPass = user.getPassword();
		String dirtyFirst = user.getFirstname();
		String dirtyLast = user.getLastname();
		String dirtyEmail = user.getEmail();
		String dirtyAge = String.valueOf(user.getAge());
		String dirtyWohnort = user.getWohnort();
		
		if(dirtyUser.equals("")) {
			invalidInput = true;
		}
		if(dirtyPass.equals("")) {
			invalidInput = true;
		}
		if(dirtyFirst.equals("")) {
			invalidInput = true;
		}
		if(dirtyLast.equals("")) {
			invalidInput = true;
		}
		if(dirtyEmail.equals("")) {
			invalidInput = true;
		}
		if(dirtyAge.equals("")) {
			invalidInput = true;
		}
		if(dirtyWohnort.equals("")) {
			invalidInput = true;
		}

		InputStream policyIn = this.getClass().getClassLoader().getResourceAsStream("antisamy-anythinggoes.xml");
		Policy policy = Policy.getInstance(policyIn);

		AntiSamy as = new AntiSamy();

		CleanResults us = as.scan(dirtyUser, policy);
		CleanResults ps = as.scan(dirtyPass, policy);
		CleanResults fs = as.scan(dirtyFirst, policy);
		CleanResults ls = as.scan(dirtyLast, policy);
		CleanResults ag = as.scan(dirtyAge, policy);
		CleanResults wo = as.scan(dirtyWohnort, policy);
		CleanResults em = as.scan(dirtyEmail, policy);
		
		String cleanPas = ps.getCleanHTML();
		byte[] hashPas = passwordToSHAHash(cleanPas);
		String stringPas = getHashAsHexString(hashPas);
		
		user.setUsername(us.getCleanHTML());
		user.setPassword(stringPas);
		user.setFirstname(fs.getCleanHTML());
		user.setLastname(ls.getCleanHTML());
		int cleanAge = Integer.parseInt(ag.getCleanHTML());
		user.setAge(cleanAge);
		user.setWohnort(wo.getCleanHTML());
		user.setEmail(em.getCleanHTML());
		
		
		model.addAttribute("password", cleanPas);
		
		
		
		for(User u : ilist) {
			if(user.getUsername().equals(u.getUsername())) {
				invalidInput = true;
			}
		}
		
		if (us.getNumberOfErrors() > 0) {
			invalidInput = true;
		}
		
		if (ps.getNumberOfErrors() > 0) {
			invalidInput = true;
		}
		
		if (fs.getNumberOfErrors() > 0) {
			invalidInput = true;
		}
		
		if (ls.getNumberOfErrors() > 0) {
			invalidInput = true;
		}
		
		if (em.getNumberOfErrors() > 0) {
			invalidInput = true;
		}
		
		if (ag.getNumberOfErrors() > 0) {
			invalidInput = true;
		}
		
		if (wo.getNumberOfErrors() > 0) {
			invalidInput = true;
		}
		
		
		if(invalidInput) {
			
			model.addAttribute("validInput", "Username bereits vergeben oder unerlaubte Eingabe,\nkontrollieren sie ob felder alle gefüllt sind");
			return "account";
		} else {
			
			model.addAttribute("loginMessage", "Sie haben erfolgreich einen Konto erstellt");
			userRepository.save(user);
			return "userPage";
		}
		
		

	}
	
	@GetMapping("/getCartItems")
	public String getCartItems(@ModelAttribute Cart cart, Model model) {
		
		
		model.addAttribute("cart", cart);
		model.addAttribute("cartList", cart.getList());
		
		return "webshop";
	}

	@PostMapping("/getShop/{id}")
	public String showArticles(@PathVariable("id") Long id, Article article, Model model) {

		
		User user = null;
		Iterable<Article> list = articleDAO.getArticles();
		Iterable<User> ilist = userRepository.findAll();
		Cart cartList = cartDAO.getCardById(1);
		cartList.getList().clear();
		
		for(User u : ilist) {
			if(u.getUserid() == id) {
				user = u;
			}
		}
		
		
		for(Article a : articleDAO.getArticles()) {
			a.setAmount(1);
		}
		
		Cart cart = cartDAO.getCardById(1);
		
		cart.setUser(user);
		model.addAttribute("user", user);
		model.addAttribute("articleList", list);
		model.addAttribute("cart", cart);

		return "webshop";

	}
	
	@PostMapping("/userCart/{id}")
	public String userCart(@PathVariable("id") Long id, @ModelAttribute User user,Article article, Model model) {
		
		Cart cart = cartDAO.getCardById(1);
		User userfound = cart.getUser();

		
		double betrag = cart.getGesamtBetrag();
		
		model.addAttribute("total", betrag);
		model.addAttribute("cartList", cart.getList());
		model.addAttribute("user", userfound);
		

		return "userCart";

	}
	
	@PostMapping("/bezahlen")
	public String bezahlen(@ModelAttribute User user, Model model) {
		
		Cart cart = cartDAO.getCardById(1);
		User userfound = cart.getUser();
		double  betrag = cart.getGesamtBetrag();
		
		model.addAttribute("paymentMessage", "Sie haben den Betrag von" + betrag + "Fr.");
		model.addAttribute("cartList", cart.getList());
		model.addAttribute("user", userfound);
		model.addAttribute("total", betrag);
		
		
		return "userCart";
		
	}
	
	@GetMapping("/addToCart/{id}")
	public String addToCart(@PathVariable("id") int id, @ModelAttribute User user, Model model) {
		
		Long userid = user.getUserid();
		
		
		Iterable<User> ilist = userRepository.findAll();
		
		
		Article ar = articleDAO.getArticleById(id);
		Cart cart = cartDAO.getCardById(1);
		
		for(User u : ilist) {
			if(u.getUserid()  == userid) {
				user = u;
			}
		}
		
		if(cart.getList().contains(ar)) {
			for(Article a : cart.getList()) {
				if(a.equals(ar)) {
					a.setAmount(a.getAmount()+1);
				}
			}
			
		} else {
			cart.addArticle(ar);
		}
		
		
		
		double betrag = cart.getGesamtBetrag();
		
		Iterable<Article> list = articleDAO.getArticles();
		
		model.addAttribute("user", cart.getUser());
		model.addAttribute("total", betrag);
		model.addAttribute("articleList", list);
		model.addAttribute("cartList", cart.getList());
		
		
		return "webshop";
	}
	
	
	
	@GetMapping("/removeFromCart/{id}")
	public String removeFromCart(@PathVariable("id") int id, @ModelAttribute User user, Model model) {
		
		
		boolean amountLess = false;
		Article ar = articleDAO.getArticleById(id);
		
		Cart cart = cartDAO.getCardById(1);
		
		
		for(Article a : cart.getList()) {
			if(a.equals(ar)) {
				if(a.getAmount() > 1) {
					a.setAmount(a.getAmount()-1);
				} else {
					amountLess = true;
				}

			}
			
		}
		if(amountLess) {
			cart.removeArticle(ar);
		}
		
		
		Iterable<Article> list = articleDAO.getArticles();
		
		double betrag = cart.getGesamtBetrag();
	
		
		model.addAttribute("user", cart.getUser());
		
		model.addAttribute("total", betrag);
		model.addAttribute("articleList", list);
		model.addAttribute("cartList", cart.getList());
		
		return "webshop";
	}
	
	//Broken access control wir haben, diesen Teil weggelassen , 
	//weil man broken access control machen kann und dann beliebig Usern löschen un bearbeitern
	
	/*
	@GetMapping("/deleteUser/{id}")
	public String deletePokemon(@PathVariable("id") long id, Model model) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		userRepository.delete(user);
		model.addAttribute("errorLogin", "Sie haben ihren account gelöscht");

		return "index";
	}

	@GetMapping("/editUser/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));

		model.addAttribute("user", user);
		return "update-user";
	}

	@PostMapping("/updateUser/{id}")
	public String updateCustomer(@PathVariable("id") long id, @Valid User user, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			user.setUserid(id);
			return "update-user";
		}

		userRepository.save(user);
		//model.addAttribute("pokemonList", userRepository.findAll());
		return "index";
	}
	*/


}
