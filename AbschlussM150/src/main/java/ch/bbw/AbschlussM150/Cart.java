package ch.bbw.AbschlussM150;

import java.util.ArrayList;

public class Cart {

	
	private int id;
	private ArrayList<Article> list;
	private User user;
	
	
	public Cart() {
		this.list = new ArrayList<Article>();
		
	}

	
	public Cart(int id, ArrayList<Article> list) {
		
		this.id = id;
		this.list = new ArrayList<Article>();
		
	}
	
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public void addArticle(Article article) {
		
		list.add(article);
	}
	
	public void removeArticle(Article article) {
		
		list.remove(article);
	}
	
	public double getGesamtBetrag() {
		double betrag = 0;
		
		for(Article a : list) {
			betrag += (a.getPrice() * a.getAmount());
		}
		
		return betrag;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ArrayList<Article> getList() {
		return list;
	}
	public void setList(ArrayList<Article> list) {
		this.list = list;
	}
	
	
	
	
}
