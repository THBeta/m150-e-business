package ch.bbw.AbschlussM150;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.stereotype.Service;


@Service
public class CartDAO {
	
private HashMap<Integer, Cart> carts;
	
	public CartDAO() {
		carts = new HashMap<>();
		
		carts.put(1, new Cart());
	}
	
	public Cart createCart() {
		Cart cart = new Cart();
		int i = carts.size();
		
		carts.put(i, cart);
		return cart;
	}

	public Cart getCardById(int id) {
		Cart cart = null;
		
		for (Map.Entry<Integer, Cart> entry : carts.entrySet()) {
			if(entry.getKey() == id) {
				cart = entry.getValue();
			}
			
		}
		
		return cart;
		
	
	}
	
	

}
