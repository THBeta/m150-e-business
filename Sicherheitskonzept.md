# M150-E-Business

M150 Abschlussprojekt


**Mögliche Sicherheitslücken**

**Organisatorische Mängel:**

Da wir ein sehr kleines Team sind und über Discord direkt miteinander reden, können Organisatorische Mängel sehr gut verhindert werden.

**Menschliche Fehlhandlungen:**

Um menschliche Fehlhandlungen zu verhindern kontrollieren wir zum Beispiel den Code des anderen und testen die Arbeit des anderen Teammitglieds.
Wir erstellen jeweils kleine Testprotokolle, damit wir diese am Teammitglied weitergeben können und sie können, dann die aufgetretene Fehler beheben.

**Vorsätzliche Handlungen:**

Bei uns hat zum Beispiel nur Thomas alle rechte, da er der Chefdevelopperist.
Wir veteilen Admin rechte nur an jene welche vertraubar sind und zudem öffnen wir "kritische" Dateien immer zuerst in einer VM, damit 
das Virus die Virtuelle machine zuerst zerstört und unsere Server/ Computer überlebt

**Technisches Versagen:**

Wir erstellen jede Woche ein Backup, damit die Schäden welches bei Versagen erzeugt werden reduzieren können. 
Wir überprüfen jedes Jahr alle Geräte, ob diese noch einsazfähig sind, wenn nein, dann werden die Geräte unverzüglich ersetzt durch neue Geräte. 
Zudem werden wir alle 5 Jahre die Technische Geräte ersetzten durch neue Geräte, wenn diese nicht mehr eunsatzfähig sind.

Die Datenintegrität wird auch jede 3 Monate überprüft ob diese Stimmen wenn nicht, dann werden die Daten sofort auf dem Aktuellen Stand gesetzt.
Zudem wird überrprüft ob die Komponenten gut miteinander kommunizieren können und das diese gut programmiert worden und richtig konfiguriert worden.
Wenn dies aber nicht der Fall ist, dann wird unverzüglich diese Ersetz bzw. korriegiert oder neu konfiguriert.

**Höhere Gewalt:**

Wir beten zu Gott und hoffen, dasss es keine Sintflutgibt oder andere Naturkatastrophen gibt.
Aber wir werden trozdem mehrere Backups haben welche nicht nur an einem Ort gepeichert werden, 
damit kann verheindert werden, dass es zu einem unerwarteten Server crash kommen kann und unsere Webseite auserbetrieb ist.


