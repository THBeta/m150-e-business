**Theori Kunden Dateien vewalten und als Dateien anzeigen**

[SQL daten in csv umwandeln](https://www.tutorialgateway.org/export-data-from-sql-to-csv/)

Wir haben uns bei dieses Ziel auch entschieden dieser nur theoretisch um zusetzen. Da wir mehrere Projektabgaben erledigen mussten und 
da der Abgabetermin verschoben wurde.

**Kunden Daten in Dateine einsetzen**

Wir hatten es am Anfang versucht umzusetzten indem wir die Daten in einen CSV datei ein zu setzten. Aber da wir die Verbindung zum frontend 
nicht herstellen konnten haben wir dieses Ziel weggelassen.Wir waren uns zude nicht sicher, ob diese art von Speicherungen legal ist,
da CSV nicht wirklich sicher sind und jeder person eigentlich diese Datei öffnen und sehr einfach lesen kann.

**Kunde Dateien verwalten**

[Dateien Speicherung mit Spring](https://youtu.be/7svczM5Xj9o)

Zu diesem Punkt wussten wir eigentlich nicht wie es umsetzten sollte, da wir so etwas noch nie wirklich gemacht haben, zudem waren 
wir uns auch nicht sicher wie wir die Dateien verschlüsseln bzw. sicher speichern konnten. Wir haben uns ein Tutorial angeschaut,
wie man Dateien mit Spring speichert, wir hätten es ähnlich gemacht aber das jeder Datei in einem Folder gespeichert wird zu welche 
nur der jeweilige Eigentümer Zugriff hat. Zudem haben wir erfahren, dass Dateien Speicherung in SQL veraltet sei und das man heutzutage 
nur noch Dateipfäde speichert. Wir haben zuvor noch nie Dateipfäder gespeichert in Databanken und deswegen haben wir auch dieses Ziel 
nicht praktisch umgesetzt wegen zu wenig Kenntnisse und wegen der Komplexität dieser Aufgabe



