Mitglieder: Thomas Hilberink, Jonas Styger

**Unser Idee**

Bei unseren E-Business kann man Computerzubehör, Computern, Laptops, Handies und Videospiele kaufen. 
Auf die unsere Webseite ist ersichtlich wie viele vom Produkt auf lager sind und wie viel sie kostet.
Zudem erhalten die Produkte mehrere Informationen.

**Was die Seite für Funktionen beinhalten wird**

Die Usern können einen Account erstellen, worin sie ihren Email, Name, Vorname, allenfalls eine Telefonnummer und eine bevorzugte Zahlungsmethode speichern können. 
Die Transaktionen setzen wir nur teilweise um, wir werden es grösstenteils theoretisch umsetzten, aber die Webseite wird eine Teil beinhalten. 
Wir werden die Daten in einer SQL Datenbank einfügen. Wir werden Passwörtern gehashed. Die Eingaben werden auf Script tags und weitere gefahren überprüft. 
Die Datei Bereitstellungen werden wir nur theoretisch umsetzten, da es ziemlich kompliziert werden kann und es allenfalls kosten pflichtig wird. 
Das was zu kostenpflichtig wird und zu kompliziert wird werden wir theoretisch umsetzen.

Da wir keinen Server haben wird es kompliziert werden alles perfekt umzusetzen.

**Zugriff**

Jeder User hat nur Berechtigungen seinen eigenen Account und Dateien zu ändern

**Sicherheitsvorsorgen**

Wir haben die Passwörter als hash gespeichert.
Man kann die Userdaten nicht ändern, da man ansonsten Broken access control ausführe konnte